const myDoubles = [1.2, 56.0, 3, 8.99, 9.0, 4.7];

function printIfInteger(number) {
    // See CH4 for some context on the 'Math' object.
    if (number === Math.floor(number)) {
        console.log(number);
    }
}

console.log("Integers:");
myDoubles.forEach(printIfInteger); // Named function

console.log("Decimals:"); // Lambda
myDoubles.forEach(n => {
    if (n !== Math.floor(n)) {
        console.log(n);
    }
});
